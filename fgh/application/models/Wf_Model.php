<?php



class Wf_Model extends CI_Model

{
    public $table_name;
    public $table_name1;
    public $primary_key;
    function __construct() {
        // parent::Model();
        parent::__construct();
        $this->table_name = "workflow";
        $this->table_name1 = "wf_record";
        $this->primary_key = "id";
    }
    public function create_wf($data)
    {

        // $result = $this->db->insert($this->table_name, $data); 
        $this->db->insert($this->table_name, $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;

    }
    public function get_wf($data = null){
      if($data){
        $this->db->where($data);
      }  
        $query = $this->db->get($this->table_name);
        return $query->row_array();
    }
    public function edit_wf($data, $wf_id){
        $this->db->where("id", $wf_id);
        return $this->db->update($this->table_name, $data);
    }
      public function attach_to_record($data){
        $result = $this->db->insert($this->table_name1, $data); 
          
      }  
}
?>