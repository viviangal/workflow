<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.6.21/dist/css/uikit.min.css" />

<!-- UIkit JS -->
        <script src="https://cdn.jsdelivr.net/npm/uikit@3.6.21/dist/js/uikit.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/uikit@3.6.21/dist/js/uikit-icons.min.js"></script>
        <style>
            .main{
              margin-top:30px; 
              width: 100%; 
            }
            .btn-create{
                margin-top:30px;
                border-radius:20px;
            }
            .uk-card{
                border-left:1px solid blue;
            }
        </style>
    </head> 
    <body>
        <div class="main uk-flex  uk-flex-center">
        <div class="uk-grid-column-default uk-grid-row-large uk-child-width-1-2@s " uk-grid>
        <?php
  $i=1;
  foreach($data as $row)
  {
    
   echo ' <div>
   <div class="uk-card uk-card-default">
   <div class="uk-card-header">
       <div class="uk-grid-small uk-flex-middle" uk-grid>
           <div class="uk-width-auto">
               <img class="uk-border-circle" width="40" height="40" src="images/avatar.jpg">
           </div>
           <div class="uk-width-expand">
               <h3 class="uk-card-title uk-margin-remove-bottom">Title</h3>
               <p class="uk-text-meta uk-margin-remove-top"></p>
           </div>
       </div>
   </div>
   <div class="uk-card-body">
   <!.. add a loop here..!>
   <ul class="uk-list uk-list-striped">
   <li>List item 1</li>
   <li>List item 2</li>
   <li>List item 3</li>
</ul>
   </div>
   <div class="uk-card-footer">
       <a href="#" class="uk-button uk-button-text">Read more</a>
   </div>
</div>

    </div>';
   

  }?>
 </div>
   
    </div>
   


<div class="uk-position-bottom-center">
<button class="uk-button uk-button-default  uk-button-small btn-create">CREATE NEW WORKFLOW</button>

</div>
    </body>
</html>