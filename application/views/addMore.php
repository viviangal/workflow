<!DOCTYPE html>
<html>
<head>
    <title>PHP Codeigniter - Dynamically Add or Remove input fields using JQuery</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.6.21/dist/css/uikit.min.css" />
   
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-p34f1UUtsS3wqzfto5wAAmdvj+osOnFyQFpp4Ua3gs/ZVWx6oOypYoCJhGGScy+8" crossorigin="anonymous"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>


<div class="container">
    <h2 align="center">PHP - Dynamically Add or Remove input fields using JQuery</h2>  
    <div class="form-group">
         <form name="add_name" method="POST" action="<?php echo base_url();?>index.php/AddMoreController/storepost">

            <div class="table-responsive">  
                <table rules="none" class="table table-bordered" id="dynamic_field">  
                    <tr>  
                        <td><input type="text" name="pname[]" placeholder="Enter your Name" class="form-control name_list" required="" /></td>  
                        <td><input type="date" name="date[]" placeholder="Enter your Due date" class="form-control name_list" required="" /></td>
                        <td style="border:1px solid white;"><button type="button" name="add" id="add"  class="btn btn-success">Add More</button></td>  
                    </tr>  
                </table>  
              
             <input type="submit" name="submit" id="submit" class="btn btn-info" value="Submit" />  
            </div>

         </form>  
    </div> 
</div>


<script type="text/javascript">
    $(document).ready(function(){      
      var i=1;  


      $('#add').click(function(){  
           i++;  
           $('#dynamic_field').append('<tr id="row'+i+'" class="dynamic-added"><td><input type="text" name="pname[]" placeholder="Enter your Name" class="form-control name_list" required /></td><td><input type="text" name="date[]" placeholder="Enter task Due date" class="form-control name_list" required /></td><td style="border:1px solid white;"><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');  
      });


      $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });  


    });  
</script>
</body>
</html>
