<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.6.21/dist/css/uikit.min.css" />

<!-- UIkit JS -->
        <script src="https://cdn.jsdelivr.net/npm/uikit@3.6.21/dist/js/uikit.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/uikit@3.6.21/dist/js/uikit-icons.min.js"></script>
        <style>
        body{
           background: rgb(232,232,232);
           padding-top: 50px;
        }
            .main{
              margin:30px; 
              
              width:80%;

            }
            h4{
                color:blue;
            }
            .btn-create{
                margin-top:30px;
                border-radius:20px;
            }
            .uk-card{
                /* border-left:1px solid blue; */
            }
            .uk-card-large .uk-card-body, .uk-card-large.uk-card-body{
                padding: 15px 15px 15px 55px;
            }
        </style>
    </head> 
    <body>
        <div class="main">
        <div class="uk-flex uk-flex-center">
        <div class="uk-grid-column-medium uk-grid-row-small uk-child-width-1-2@s" uk-grid>
        <?php
  $i=1;
  $path = "index.php/AddMoreController/createnewwf";
  foreach($data as $reted)
  {
   
    
   echo ' <div>
   <div class="uk-card uk-card-default uk-card-large">
   <div class="uk-card-header">
       <div class="uk-grid-small uk-flex-middle" uk-grid>
      
           <div class="uk-width-expand">
               <h4 class="uk-card-title uk-margin-remove-bottom" style="color:blue">'.$reted['workflow_name'].'</h4>
               <p class="uk-text-meta uk-margin-remove-top">12/12/2020</p>
           </div>
       </div>
   </div>
   <div class="uk-card-body">
   <ul uk-accordion>
   <li class="">
   <a class="uk-accordion-title" href="#">WorkFlow steps</a>
   <div class="uk-accordion-content">
   <ul class="uk-list uk-list-decimal">';
   
      
   
 foreach($transitions as $trans){
     if($trans['wf_id']== $reted['id']){
    //  echo '<li>'.$trans['id'].'</li>';
     echo ' 
    
    
            <li>List item </li>
          
       
     ';
 
     } 
   
   
  }
  echo ' </ul> </div></li> </ul>
   </div>
   <div class="uk-card-footer ">
   <div class="uk-panel">
  
   <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
   <a class="md-fab md-fab-accent md-fab-wave md-fab-success" href="'.$path.'">
   Add
</a>
</div>
   </div>
</div>

    </div>';
   

  }?>
 </div>
   
    </div>
<!-- <button class="uk-button uk-button-default  uk-button-small btn-create"onclick="">CREATE NEW WORKFLOW</button> -->
   
<div data-uk-tooltip title="Create Work flow">
								<a class="md-fab md-fab-accent md-fab-wave md-fab-success" href="<?php echo base_url('/index.php/AddMoreController/createnewwf');?>">
									<i class="material-icons">&#xE145;</i>
								</a>
							</div>

<div class="uk-position-bottom-center">
    <!-- <div>
        <div class="uk-card uk-card-default uk-card-large  uk-card-body">
        
        
        
        </div> -->
    </div>
   
</div>
</div>

</div>
    </body>
</html>
<div class="uk-flex uk-flex-center uk-grid-column-default uk-grid-row-large uk-child-width-1-2@s " uk-grid>
      